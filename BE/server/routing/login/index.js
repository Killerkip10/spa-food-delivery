const routing = require('express').Router();
const controller = require('./controller');

routing.route('/login')
  .post((...args)=>controller.login(...args));
routing.route('/registration')
  .post((...args)=>controller.registration(...args));

module.exports = routing;