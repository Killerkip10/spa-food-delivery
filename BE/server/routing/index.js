const config = require('./../../config');

module.exports = (app)=>{
  app.use('/api/authorization', require('./login'));
  app.use('/api/test', (req, res, next)=>{
    res.send('Work');
  });
};