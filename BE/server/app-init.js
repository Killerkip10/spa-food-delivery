const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const cookieParser = require('cookie-parser');

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(cookieParser());
app.use(morgan('combined'));

require('./routing')(app);

app.use(express.static(__dirname + '/../../FE/dist/FE'));

app.use((err, req, res, next)=>{
  if(err.status !== 404) console.error(err);
  res.sendStatus(err.status || 500);
});

module.exports = app;