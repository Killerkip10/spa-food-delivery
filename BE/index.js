const app = require('./server/app-init');
const config = require('./config');

app.listen(config.port, ()=>{
  console.log(`Server has started on ${config.port} port`);
});