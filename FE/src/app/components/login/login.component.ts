import {Component} from "@angular/core";

import {Login} from '../../models';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent{
  constructor(){

  }
  public onSubmit(loginForm){
    console.log(<Login>loginForm.value);
  }
}
