import {NgModule} from "@angular/core";
import {Routes, RouterModule} from '@angular/router';

import {LoginPageComponent} from './page/login-page.component';

import {LoginModule} from '../../components';

const routes: Routes = [
  {
    path:'login',
    component:LoginPageComponent,
    pathMatch:'full'
  }
];

@NgModule({
  declarations:[
    LoginPageComponent
  ],
  imports:[
    LoginModule,

    RouterModule.forRoot(routes)
  ],
  providers:[

  ],
  exports:[
    LoginPageComponent
  ]
})
export class LoginPageModule{}
